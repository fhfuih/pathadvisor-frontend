# Street View Feature

Open floor 2 map for example. Drag the man icon to the map. It will be pinned to the nearest node with a panorama and a panorama will be shown. You can drag & click the map to navigate, or use arrow keys. Note when using arrow keys, you muist click on the panorama first to focus the keyboard events.
