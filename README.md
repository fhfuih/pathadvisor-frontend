# HKUST Path advisor frontend

To develop a plugin for this app, please see this [documentation](https://pathadvisor.ust.hk/docs)

To run the app locally, you will need node 8.x and npm 5.x installed. Please clone this repository and run
`npx bolt` then `npm start` in the root directory of the app. You should able to access the app locally at http://localhost:3000


## Street View Feature

See [street view docs](README.streetview.md)
